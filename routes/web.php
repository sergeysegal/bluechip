<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use \App\Http\Controllers;

Route::get('/', function () {
    return view('welcome');
});

Route::auth();
Route::get('/home','HomeController@index');

Route::post('/createpost', [
    'uses' => 'PostController@createPost',
    'as' => 'post.create'
]);

Route::get('/deletepost/{post_id}', [
    'uses' => 'PostController@deletePost',
    'as' => 'post.delete'
]);

Route::get('/createpost','HomeController@index');