@extends('layouts.app')

@section('content')

    <div class="container">
            <section class="row new-post justify-content-center">
                <div class="col-md-6 col-md-offset-3">
                    <header><h3>Create Post</h3></header>
                    @include('includes.message')
                    <form action="{{route('post.create')}}" method="post">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input class="form-control" type="text" name="user_name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input class="form-control" type="text" name="user_email" id="email">
                        </div>
                        <div class="form-group">
                            <label for="body">Message:</label>
                            <textarea class="form-control" name="body" id="new-post" rows="5" placeholder="Type your message here"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Create Post</button>
                        <input type="hidden" value="{{Session::token() }}" name="_token">
                    </form>
                </div>
            </section>
            <br>
            <section class="row posts justify-content-center">
                <div class="col-md-6 col-md-3-offset">
                    <header><h3>All Posts</h3></header>
                    @foreach($posts as $post)
                        <article class="post">
                            <p>{{$post->body}}</p>
                            <div class="info">
                                Entered Data -> Name: {{$post->user_name}} | Email: {{$post->user_email}} <br>
                                User Data -> Name: {{$post->user->name}} | Email: {{$post->user->email}}
                            </div>
                            <div class="interaction">
                                {{--<a href="#">Edit</a>--}}
                                <a href="{{route('post.delete',['post_id' => $post->id])}}">Delete</a>
                            </div>
                        </article>
                    @endforeach
                </div>
            </section>
            <div class="row justify-content-center">
                {!! $posts->links(); !!}
            </div>
    </div>
@endsection
