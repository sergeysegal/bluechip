<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected  $table='posts'; //define which table model should refer to
    protected  $primaryKey = 'id'; //define what primary key is called

    protected $fillable = [  //allow for mass assignment
        'user_id',
        'user_name',
        'user_email',
        'body'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
