<?php
/**
 * Created by PhpStorm.
 * User: sergey.segal
 * Date: 1/21/2019
 * Time: 8:34 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function createPost(Request $request)
    {
        $this->validate($request, [
            'user_name'=>'required',
            'user_email'=>'required|email',
            'body' => 'required|max:1000'
        ]);
        $post = new Post();
        $post->user_name = $request['user_name'];
        $post->user_email = $request['user_email'];
        $post->body = $request['body'];
        $message = "There was an error";
        if($request->user()->posts()->save($post)){
            $message = "Post successfully created!";
        }
//        return view('home',['posts' => $posts]);
        return redirect('home')->with(['message'=>$message]);
    }

    public function deletePost($post_id)
    {
        $post = Post::findOrFail($post_id);
        $post->delete();
        return redirect('home')->with(['message'=>'Post successfully deleted!']);
    }
}